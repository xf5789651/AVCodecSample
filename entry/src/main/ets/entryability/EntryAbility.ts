/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIAbility from '@ohos.app.ability.UIAbility'
import window from '@ohos.window'
import abilityAccessCtrl from '@ohos.abilityAccessCtrl'
import Logger from '../common/utils/Logger'

function requestPre() {
  let atManager = abilityAccessCtrl.createAtManager()
  try {
    atManager.requestPermissionsFromUser(globalThis.context,
      ['ohos.permission.MEDIA_LOCATION', 'ohos.permission.READ_MEDIA',
        'ohos.permission.WRITE_MEDIA', 'ohos.permission.CAMERA',])
      .then((data) => {
        Logger.info("requestPre() data: " + JSON.stringify(data))
      }).catch((err) => {
      Logger.info("requestPre() data: " + JSON.stringify(err))
    })
  } catch (err) {
    Logger.error("requestPre() data: " + JSON.stringify(err))
  }
}

export default class EntryAbility extends UIAbility {
  onCreate(want, launchParam) {
    Logger.info('Ability onCreate')
    globalThis.context = this.context
    requestPre()
  }

  onDestroy() {
    Logger.info('Ability onDestroy')
  }

  onWindowStageCreate(windowStage: window.WindowStage) {
    // Main window is created, set main page for this ability
    Logger.info('Ability onWindowStageCreate')

    windowStage.getMainWindowSync().setWindowKeepScreenOn(true)
    windowStage.loadContent('pages/Index', (err, data) => {
      if (err.code) {
        Logger.error('Failed to load the content. Cause: %{public}s', JSON.stringify(err) ?? '')
        return
      }
      Logger.info('Succeeded in loading the content. Data: %{public}s', JSON.stringify(data) ?? '')
    })
  }

  onWindowStageDestroy() {
    // Main window is destroyed, release UI related resources
    Logger.info('Ability onWindowStageDestroy')
  }

  onForeground() {
    // Ability has brought to foreground
    Logger.info('Ability onForeground')
  }

  onBackground() {
    // Ability has back to background
    Logger.info('Ability onBackground')
  }
}
