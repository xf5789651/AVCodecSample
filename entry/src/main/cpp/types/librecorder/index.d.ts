export const initNative: (fd: number, videoCodecMime: string, width: number, height: number,
                          frameRate: number, isHDRVivid: number, bitRate: number) => Promise<Response>

export const startNative: () => void

export const stopNative: () => Promise<Response>

export class Response {
  code: number
  surfaceId: string
}